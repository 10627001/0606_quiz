package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int sum=0,  mul=1, cut=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("請輸入一個數");
        int num = scanner.nextInt();
        if(num < 100 || num > 999) {System.out.println("錯誤的值");}
        else
        {
            while(num != 0){
                sum += num % 10;
                mul *= num % 10;
                if(num / 10 != 0) {cut -= num % 10;}
                else {cut += num % 10;}
                num /= 10;
            }
            System.out.println("每位數和=" + sum);
            System.out.println("每位數積=" + mul);
            System.out.println("每位數差=" + cut);
        }

    }
}
